package main

import (
	"github.com/jackc/pgx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"log"
	"music-storage/internal/auth"
	"music-storage/internal/endpoint"
	"music-storage/internal/logger"
	"music-storage/internal/migrate"
	"music-storage/internal/music"
	"music-storage/internal/repo"
	"os"
	"os/signal"
)

var (
	httpHandler *endpoint.HttpHandler
)

var (
	dbConn       *pgx.Conn
	authorsTable *repo.AuthorsTable
	albumsTable  *repo.AlbumsTable
	songsTable   *repo.SongsTable

	musicService *music.Service
	authorizer   *auth.Authorizer
)

func main() {
	logger.Initialize()

	setupConfig()
	setupDatabase()
	setupTables()

	musicService = music.NewService(songsTable)
	authorizer = auth.NewAuthorizer()

	httpHandler = endpoint.NewHttpHandler(authorsTable, albumsTable, songsTable, musicService, authorizer)
	go func() {
		logrus.Info("Server was started")

		s := &fasthttp.Server{
			Handler:           httpHandler.Handle,
			StreamRequestBody: true,
		}
		err := s.ListenAndServe("0.0.0.0:8002")
		if err != nil {
			logrus.Warn(err.Error())
		}
	}()

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	<-sigChan
}

func setupConfig() {
	viper.SetConfigName("configuration")
	viper.SetConfigType("yml")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		logrus.Fatal(err.Error())
	}

	// database default values
	viper.SetDefault("sql.hostname", "localhost")
	viper.SetDefault("sql.port", "5432")
	viper.SetDefault("sql.user", "postgres")
}

func setupDatabase() {
	host := viper.GetString("sql.hostname")
	port := viper.GetUint32("sql.port")
	user := viper.GetString("sql.user")
	password := viper.GetString("sql.password")
	databaseName := viper.GetString("sql.database")

	var err error
	dbConn, err = pgx.Connect(pgx.ConnConfig{
		Host:         host,
		Port:         uint16(port),
		Database:     databaseName,
		User:         user,
		Password:     password,
		CustomCancel: func(_ *pgx.Conn) error { return nil },
	})
	if err != nil {
		log.Fatalln("Error occurred during opening database connection: ",
			err.Error())
	}

	err = migrate.Migrate(dbConn, "./migrations")
	if err != nil {
		logrus.Fatal(err.Error())
	}
}

func setupTables() {
	/*_, err := dbConn.Exec(`DECLARE song_cursor CURSOR FOR SELECT id, name FROM songs;`)
	if err != nil {
		return
	}
	dbConn.Exec(`OPEN song_cursor;`)*/

	var err error
	authorsTable, err = repo.NewAuthorsTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}

	albumsTable, err = repo.NewAlbumsTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}

	songsTable, err = repo.NewSongsTable(dbConn)
	if err != nil {
		logrus.Fatal(err.Error())
	}
}
