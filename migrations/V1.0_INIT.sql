CREATE TABLE authors
(
    id     SERIAL PRIMARY KEY,
    name   VARCHAR NOT NULL,
    bio    VARCHAR,
    images VARCHAR[]
);

CREATE TABLE albums
(
    id        SERIAL PRIMARY KEY,
    name      VARCHAR  NOT NULL,
    author_id INTEGER REFERENCES authors (id) ON DELETE CASCADE ON UPDATE CASCADE,
    type      SMALLINT NOT NULL,
    image     VARCHAR
);

CREATE TABLE songs
(
    id           SERIAL PRIMARY KEY,
    name         VARCHAR   NOT NUlL,
    author_id    INTEGER REFERENCES authors (id) ON DELETE CASCADE ON UPDATE CASCADE,
    album_id     INTEGER REFERENCES albums (id) ON DELETE CASCADE ON UPDATE CASCADE,
    duration     INTEGER   NOT NULL,
    video        VARCHAR,
    release_year SMALLINT,
    added_at     TIMESTAMP NOT NULL,
    status       SMALLINT  NOT NULL,
    image        VARCHAR,
    data         BYTEA     NOT NULL
);

