package dto

import (
	"errors"
	"music-storage/internal/repo"
)

var (
	ErrInvalidAlbumType = errors.New("invalid album type")
)

const (
	AlbumTypeAlbum  = "album"
	AlbumTypeSingle = "single"
)

func GetAlbumTypeStr(albumType int16) string {
	switch albumType {
	case repo.AlbumTypeAlbum:
		return AlbumTypeAlbum

	case repo.AlbumTypeSingle:
		return AlbumTypeSingle
	}

	return ""
}

func GetAlbumTypeInt16(albumType string) int16 {
	switch albumType {
	case AlbumTypeAlbum:
		return repo.AlbumTypeAlbum

	case AlbumTypeSingle:
		return repo.AlbumTypeSingle
	}

	return -1
}

type Album struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	AuthorId int    `json:"author_id"`
	Type     string `json:"type"`
	Image    string `json:"image"`
}

func (a *Album) From(album repo.Album) {
	a.Id = album.Id
	a.Name = album.Name
	a.AuthorId = album.AuthorId
	a.Type = GetAlbumTypeStr(album.Type)
	a.Image = album.Image
}

func (a *Album) ToRepoModel() (repo.Album, error) {
	albumType := GetAlbumTypeInt16(a.Type)
	if albumType == -1 {
		return repo.Album{}, ErrInvalidAlbumType
	}

	return repo.Album{
		Id:       a.Id,
		Name:     a.Name,
		AuthorId: a.AuthorId,
		Type:     albumType,
		Image:    a.Image,
	}, nil
}
