package search

const optionsByLeave = 5

type Bohr[T any] struct {
	root bohrVertex[T]
}

type bohrVertex[T any] struct {
	key      rune
	children []bohrVertex[T]
	value    []T
	leaves   int
}

func (b *Bohr[T]) binarySearch(key rune, children []bohrVertex[T]) (int, bool) {
	start := 0
	end := len(children)

	for end > start {
		middle := (end + start) >> 1

		k := children[middle].key
		if k < key {
			end = middle
		} else if k > key {
			start = middle + 1
		} else {
			return middle, true
		}
	}

	return start, false
}

func (b *Bohr[T]) incrementLeavesNumber(symbols []rune) {
	cur := &b.root
	for _, sym := range symbols {
		cur.leaves++
		index, ok := b.binarySearch(sym, cur.children)
		if ok {
			cur = &cur.children[index]
			continue
		} else {
			panic("no such symbol inserted")
		}
	}
}

func (b *Bohr[T]) Insert(str string) *[]T {
	symbols := []rune(str)

	cur := &b.root

	for i, sym := range symbols {
		index, ok := b.binarySearch(sym, cur.children)
		if ok {
			cur = &cur.children[index]
			continue
		}

		if len(cur.children) > 0 && len(cur.children) < index {
			cur.children = append(cur.children[:index+1], cur.children[index:]...)
			cur = &cur.children[index]
			*cur = bohrVertex[T]{
				key: sym,
			}
		} else {
			cur.children = append(cur.children, bohrVertex[T]{key: sym})
			cur = &cur.children[index]
		}

		for _, s := range symbols[i+1:] {
			cur.children = append(cur.children, bohrVertex[T]{key: s})
			cur = &cur.children[0]
		}

		b.incrementLeavesNumber(symbols)
		break
	}

	return &cur.value
}

func (b *Bohr[T]) depthTraversal(node *bohrVertex[T], totalOffset, leaves int, result []T) []T {
	for i := 0; i < len(node.children); i++ {
		result = b.depthTraversal(&node.children[i], totalOffset, leaves, result)
		if len(result) == cap(result) {
			return result
		}
	}
	offset := totalOffset / leaves

	for i := cap(result)
}

func (b *Bohr[T]) Search(str string, page, count int) []T {
	symbols := []rune(str)
	cur := &b.root

	for i, sym := range symbols {
		index, ok := b.binarySearch(sym, cur.children)
		if ok {
			cur = &cur.children[index]
			continue
		}

		if i+1 != len(symbols) {
			return nil
		}

		return nil
	}

	return cur.value
}
