package util

type Optional[T any] struct {
	value  T
	notNil bool
}

func (o *Optional[T]) Set(val T) {
	o.value = val
	o.notNil = true
}

func (o *Optional[T]) Get() *T {
	if o.notNil {
		return &o.value
	} else {
		return nil
	}
}

func (o *Optional[T]) GetRow() *T {
	o.notNil = true
	return &o.value
}

func (o *Optional[T]) SetNil() {
	o.notNil = false
}
