package music

import (
	"music-storage/internal/mp3"
	"music-storage/internal/repo"
)

const maxRequestedSongDuration = 20

type Service struct {
	songsTable    *repo.SongsTable
	songDecorator *mp3.SongDecorator
}

func NewService(songsTable *repo.SongsTable) *Service {
	return &Service{songsTable: songsTable}
}

func (s *Service) Get(songId int, start, end uint64) ([]byte, error) {
	data, err := s.songsTable.GetDataById(songId)
	if err != nil {
		return nil, err
	}

	var mp3Song *mp3.Song
	mp3Song, err = mp3.NewSong(data)
	if err != nil {
		return nil, err
	}

	var songMeta mp3.SongMeta
	songMeta, err = s.songDecorator.ReadMeta(mp3Song)
	if err != nil {
		return nil, err
	}

	if end <= start || end-start > maxRequestedSongDuration {
		end = start + 20
	}

	if start <= songMeta.Length() {
		startOffset := songMeta.GetOffsetForSecond(start)
		if end >= songMeta.Length() {
			return data[startOffset:], nil
		} else {
			endOffset := songMeta.GetOffsetForSecond(end)
			return data[startOffset:endOffset], nil
		}
	}

	return nil, nil
}
