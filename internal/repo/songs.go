package repo

import (
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx"
	"time"
)

const (
	SongStatusOk = iota
	SongStatusBanned
)

type Song struct {
	AddedAt     time.Time `json:"added_at"`
	Name        string    `json:"name"`
	Video       string    `json:"video"`
	Image       string    `json:"image"`
	Data        []byte    `json:"-"`
	Id          int       `json:"id"`
	AuthorId    int       `json:"author_id"`
	AlbumId     int       `json:"album_id"`
	Duration    uint64    `json:"duration"`
	ReleaseYear int16     `json:"release_year"`
	Status      int16     `json:"status"`
}

type SongsTable struct {
	db               *pgx.Conn
	getByAlbumIdStmt *pgx.PreparedStatement
	getByIdStmt      *pgx.PreparedStatement
	getDataByIdStmt  *pgx.PreparedStatement
	insertStmt       *pgx.PreparedStatement
	deleteByIdStmt   *pgx.PreparedStatement
}

const (
	getSongsByAlbumIdQuery = `SELECT id, name, author_id, album_id, duration, video, release_year, added_at, status, image FROM songs WHERE album_id = $1`
	getSongByIdQuery       = `SELECT id, name, author_id, album_id, duration, video, release_year, added_at, status, image FROM songs WHERE id = $1`
	getSongDataByIdQuery   = `SELECT data FROM songs WHERE id = $1`
	insertSongQuery        = `INSERT INTO songs (name, author_id, album_id, duration, video, release_year, added_at, status, image, data) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`
	deleteSongByIdQuery    = `DELETE FROM songs WHERE id = $1`
)

func NewSongsTable(db *pgx.Conn) (*SongsTable, error) {
	var (
		err              error
		getByAlbumIdStmt *pgx.PreparedStatement
		getByIdStmt      *pgx.PreparedStatement
		getDataByIdStmt  *pgx.PreparedStatement
		insertStmt       *pgx.PreparedStatement
		deleteByIdStmt   *pgx.PreparedStatement
	)

	getByAlbumIdStmt, err = db.Prepare("getAllSongsQuery", getSongsByAlbumIdQuery)
	if err != nil {
		return nil, err
	}
	getByIdStmt, err = db.Prepare("getSongByIdQuery", getSongByIdQuery)
	if err != nil {
		return nil, err
	}
	getDataByIdStmt, err = db.Prepare("getSongDataByIdQuery", getSongDataByIdQuery)
	if err != nil {
		return nil, err
	}
	insertStmt, err = db.Prepare("insertSongQuery", insertSongQuery)
	if err != nil {
		return nil, err
	}
	deleteByIdStmt, err = db.Prepare("deleteSongByIdQuery", deleteSongByIdQuery)
	if err != nil {
		return nil, err
	}

	return &SongsTable{
		db:               db,
		getByAlbumIdStmt: getByAlbumIdStmt,
		getByIdStmt:      getByIdStmt,
		getDataByIdStmt:  getDataByIdStmt,
		insertStmt:       insertStmt,
		deleteByIdStmt:   deleteByIdStmt,
	}, nil
}

func (t *SongsTable) GetByAlbumId(albumId int) ([]Song, error) {
	rows, err := t.db.Query(t.getByAlbumIdStmt.Name, albumId)
	if err != nil {
		return nil, err
	}

	var result []Song
	for rows.Next() {
		var (
			song        Song
			video       pgtype.Varchar
			releaseYear pgtype.Int2
			image       pgtype.Varchar
		)
		err = rows.Scan(&song.Id,
			&song.Name,
			&song.AuthorId,
			&song.AlbumId,
			&song.Duration,
			&video,
			&releaseYear,
			&song.AddedAt,
			&song.Status,
			&image)
		if err != nil {
			return nil, err
		}
		if video.Status == pgtype.Present {
			song.Video = video.String
		}
		if image.Status == pgtype.Present {
			song.Image = image.String
		}
		if releaseYear.Status == pgtype.Present {
			song.ReleaseYear = releaseYear.Int
		}
		result = append(result, song)
	}
	rows.Close()

	return result, rows.Err()
}

func (t *SongsTable) Insert(song Song) error {
	_, err := t.db.Exec(
		t.insertStmt.Name,
		song.Name,
		song.AuthorId,
		song.AlbumId,
		song.Duration,
		song.Video,
		song.ReleaseYear,
		song.AddedAt,
		song.Status,
		song.Image,
		song.Data,
	)

	return err
}

func (t *SongsTable) GetDataById(id int) ([]byte, error) {
	row := t.db.QueryRow(t.getDataByIdStmt.Name, id)

	var data []byte
	err := row.Scan(&data)
	return data, err
}

func (t *SongsTable) DeleteById(id int) error {
	_, err := t.db.Exec(t.deleteByIdStmt.Name, id)
	return err
}

func (t *SongsTable) GetById(id int) (Song, error) {
	row := t.db.QueryRow(t.getByIdStmt.Name, id)

	var (
		song        Song
		video       pgtype.Varchar
		releaseYear pgtype.Int2
		image       pgtype.Varchar
	)
	err := row.Scan(&song.Id,
		&song.Name,
		&song.AuthorId,
		&song.AlbumId,
		&song.Duration,
		&video,
		&releaseYear,
		&song.AddedAt,
		&song.Status,
		&image)
	if video.Status == pgtype.Present {
		song.Video = video.String
	}
	if image.Status == pgtype.Present {
		song.Image = image.String
	}
	if releaseYear.Status == pgtype.Present {
		song.ReleaseYear = releaseYear.Int
	}

	return song, err
}
