package repo

import (
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx"
)

type Author struct {
	Id     int      `json:"id"`
	Name   string   `json:"name"`
	Bio    string   `json:"bio"`
	Images []string `json:"images"`
}

type AuthorsTable struct {
	db          *pgx.Conn
	getByIdStmt *pgx.PreparedStatement
	insertStmt  *pgx.PreparedStatement
	deleteStmt  *pgx.PreparedStatement
}

const (
	getAuthorByIdQuery = `SELECT id, name, bio, images FROM authors WHERE id = $1`
	insertAuthorQuery  = `INSERT INTO authors (name, bio, images) VALUES ($1, $2, $3)`
	deleteAuthorQuery  = `DELETE FROM authors WHERE id = $1`
)

func NewAuthorsTable(db *pgx.Conn) (*AuthorsTable, error) {
	var (
		err              error
		getByIdStmt      *pgx.PreparedStatement
		insertAuthorStmt *pgx.PreparedStatement
		deleteAuthorStmt *pgx.PreparedStatement
	)

	getByIdStmt, err = db.Prepare("getAuthorByIdQuery", getAuthorByIdQuery)
	if err != nil {
		return nil, err
	}
	insertAuthorStmt, err = db.Prepare("insertAuthorQuery", insertAuthorQuery)
	if err != nil {
		return nil, err
	}
	deleteAuthorStmt, err = db.Prepare("deleteAuthorQuery", deleteAuthorQuery)
	if err != nil {
		return nil, err
	}

	return &AuthorsTable{
		db:          db,
		getByIdStmt: getByIdStmt,
		insertStmt:  insertAuthorStmt,
		deleteStmt:  deleteAuthorStmt,
	}, nil
}

func (t *AuthorsTable) GetById(id int) (Author, error) {
	row := t.db.QueryRow(t.getByIdStmt.Name, id)
	var author Author

	bio := pgtype.Varchar{}
	err := row.Scan(&author.Id, &author.Name, &bio, &author.Images)
	if bio.Status == pgtype.Present {
		author.Bio = bio.String
	}

	return author, err
}

func (t *AuthorsTable) Delete(id int) error {
	_, err := t.db.Exec(t.deleteStmt.Name, id)
	return err
}

func (t *AuthorsTable) Insert(author Author) error {
	_, err := t.db.Exec(t.insertStmt.Name, author.Name, author.Bio, author.Images)
	return err
}
