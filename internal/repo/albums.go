package repo

import (
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx"
)

const (
	AlbumTypeAlbum = iota
	AlbumTypeSingle
)

type Album struct {
	Id       int    `json:"id"`
	Name     string `json:"name"`
	AuthorId int    `json:"author_id"`
	Type     int16  `json:"type"`
	Image    string `json:"image"`
}

type AlbumsTable struct {
	db                *pgx.Conn
	getByAuthorIdStmt *pgx.PreparedStatement
	getByIdStmt       *pgx.PreparedStatement
	insertStmt        *pgx.PreparedStatement
	deleteByIdStmt    *pgx.PreparedStatement
}

const (
	getAllAuthorsAlbumsQuery = `SELECT id, name, author_id, type, image FROM albums WHERE author_id = $1`
	getAlbumByIdQuery        = `SELECT id, name, author_id, type, image FROM albums WHERE id = $1`
	insertAlbumQuery         = `INSERT INTO albums (name, author_id, type, image) VALUES ($1, $2, $3, $4)`
	deleteAlbumByIdQuery     = `DELETE FROM albums WHERE id = $1`
)

func NewAlbumsTable(db *pgx.Conn) (*AlbumsTable, error) {
	var (
		err                 error
		getAllStmt          *pgx.PreparedStatement
		getByIdStmt         *pgx.PreparedStatement
		insertAlbumStmt     *pgx.PreparedStatement
		deleteAlbumByIdStmt *pgx.PreparedStatement
	)

	getAllStmt, err = db.Prepare("getAllAuthorsAlbumsQuery", getAllAuthorsAlbumsQuery)
	if err != nil {
		return nil, err
	}
	getByIdStmt, err = db.Prepare("getAlbumByIdQuery", getAlbumByIdQuery)
	if err != nil {
		return nil, err
	}
	insertAlbumStmt, err = db.Prepare("insertAlbumQuery", insertAlbumQuery)
	if err != nil {
		return nil, err
	}
	deleteAlbumByIdStmt, err = db.Prepare("deleteAlbumByIdQuery", deleteAlbumByIdQuery)
	if err != nil {
		return nil, err
	}

	return &AlbumsTable{
		db:                db,
		getByAuthorIdStmt: getAllStmt,
		getByIdStmt:       getByIdStmt,
		insertStmt:        insertAlbumStmt,
		deleteByIdStmt:    deleteAlbumByIdStmt,
	}, nil
}

func (t *AlbumsTable) GetByAuthorId(authorId int) ([]Album, error) {
	rows, err := t.db.Query(t.getByAuthorIdStmt.Name, authorId)
	if err != nil {
		return nil, err
	}

	var result []Album
	for rows.Next() {
		var (
			a     Album
			image pgtype.Varchar
		)
		err = rows.Scan(&a.Id, &a.Name, &a.AuthorId, &a.Type, &image)
		if err != nil {
			return nil, err
		}
		if image.Status == pgtype.Present {
			a.Image = image.String
		}

		result = append(result, a)
	}
	rows.Close()

	return result, rows.Err()
}

func (t *AlbumsTable) GetById(id int) (Album, error) {
	row := t.db.QueryRow(t.getByIdStmt.Name, id)

	var (
		a     Album
		image pgtype.Varchar
	)
	err := row.Scan(&a.Id, &a.Name, &a.AuthorId, &a.Type, &image)
	if err != nil {
		return Album{}, err
	}
	if image.Status == pgtype.Present {
		a.Image = image.String
	}

	return a, nil
}

func (t *AlbumsTable) Insert(album Album) error {
	_, err := t.db.Exec(t.insertStmt.Name, album.Name, album.AuthorId, album.Type, album.Image)
	return err
}

func (t *AlbumsTable) Delete(id int) error {
	_, err := t.db.Exec(t.deleteByIdStmt.Name, id)
	return err
}
