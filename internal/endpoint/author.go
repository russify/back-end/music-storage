package endpoint

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"music-storage/internal/repo"
)

func (h *HttpHandler) deleteAuthors(ctx *fasthttp.RequestCtx) {
	authorId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	err = h.authorsTable.Delete(authorId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

func (h *HttpHandler) getAuthors(ctx *fasthttp.RequestCtx) {
	authorId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	var author repo.Author
	author, err = h.authorsTable.GetById(authorId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	var row []byte
	row, err = json.Marshal(author)
	ctx.SetContentType("application/json")
	ctx.SetBody(row)
}

func (h *HttpHandler) postAuthors(ctx *fasthttp.RequestCtx) {
	author := repo.Author{}
	err := json.Unmarshal(ctx.PostBody(), &author)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	if author.Name == "" {
		writeError(ctx, "author's name was not provided", fasthttp.StatusBadRequest)
		return
	}

	err = h.authorsTable.Insert(author)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNoContent)
}
