package endpoint

import (
	"encoding/json"
	"github.com/fasthttp/websocket"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"time"
	"unicode"
)

func waitClose(conn *websocket.Conn) {
	_, _, err := conn.ReadMessage()
	if err != nil {
		if ok := websocket.IsUnexpectedCloseError(err, websocket.CloseNormalClosure); ok {
			logrus.Warn(err.Error())
		}
	}
}

func sendCloseAndWait(conn *websocket.Conn, closeCode int) {
	err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(closeCode, ""))
	if err != nil {
		logrus.Warn(err.Error())
		return
	}

	waitClose(conn)
}

func sendCloseAndWaitTimeout(conn *websocket.Conn, closeCode int, timeout time.Duration) {
	err := conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(closeCode, ""))
	if err != nil {
		logrus.Warn(err.Error())
		return
	}

}

func writeObject(ctx *fasthttp.RequestCtx, obj any, status int) {
	row, err := json.Marshal(obj)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(status)
	ctx.Response.Header.Set(fasthttp.HeaderContentType, "application/json")
	_, _ = ctx.Write(row)
}

type errorResponse struct {
	Error string `json:"error"`
}

func writeError(ctx *fasthttp.RequestCtx, message string, status int) {
	response := errorResponse{Error: message}
	row, err := json.Marshal(&response)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(status)
	ctx.Response.Header.Set(fasthttp.HeaderContentType, "application/json")
	_, _ = ctx.Write(row)
}

func checkGroupName(ctx *fasthttp.RequestCtx, groupName string) bool {
	if groupName == "" {
		writeError(ctx, "group name was not provided", fasthttp.StatusBadRequest)
		return false
	}
	if len(groupName) > 256 {
		writeError(ctx, "too long group name", fasthttp.StatusBadRequest)
		return false
	}
	for _, r := range []rune(groupName) {
		if unicode.IsSpace(r) || r == '/' || r == '\\' || r == ',' || r == '"' || r == '\'' {
			writeError(ctx, "group name must not contain spaces, '\\', '/' etc", fasthttp.StatusBadRequest)
			return false
		}
	}

	return true
}

func addCorsHeaders(ctx *fasthttp.RequestCtx) {
	ctx.Response.Header.Set("Access-Control-Allow-Origin", "*")
	ctx.Response.Header.Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH")
	ctx.Response.Header.Set("Access-Control-Allow-Headers", "*")
}
