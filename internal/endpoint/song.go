package endpoint

import (
	"encoding/json"
	"github.com/fasthttp/websocket"
	"github.com/jackc/pgx"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"io"
	"mime/multipart"
	"music-storage/internal/mp3"
	"music-storage/internal/repo"
	"strconv"
	"time"
)

func (h *HttpHandler) readSongData(ctx *fasthttp.RequestCtx, file *multipart.FileHeader) ([]byte, uint64) {
	dataFile, err := file.Open()
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return nil, 0
	}

	var data []byte
	data, err = io.ReadAll(dataFile)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return nil, 0
	}

	var song *mp3.Song
	song, err = mp3.NewSong(data)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return nil, 0
	}

	var duration uint64
	duration, err = h.songDecorator.AddMeta(song)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return nil, 0
	}

	return song.File, duration
}

func (h *HttpHandler) readSong(ctx *fasthttp.RequestCtx, values map[string][]string) (repo.Song, bool) {
	song := repo.Song{}

	if arr, ok := values["name"]; ok {
		song.Name = arr[0]
	} else {
		writeError(ctx, "name wasn't provided", fasthttp.StatusBadRequest)
		return repo.Song{}, false
	}

	if arr, ok := values["video"]; ok {
		song.Video = arr[0]
	}
	if arr, ok := values["image"]; ok {
		song.Image = arr[0]
	}

	if arr, ok := values["author_id"]; ok {
		id, err := strconv.Atoi(arr[0])
		if err != nil {
			writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
			return repo.Song{}, false
		}
		song.AuthorId = id
	} else {
		writeError(ctx, "author's id wasn't provided", fasthttp.StatusBadRequest)
		return repo.Song{}, false
	}
	if arr, ok := values["album_id"]; ok {
		id, err := strconv.Atoi(arr[0])
		if err != nil {
			writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
			return repo.Song{}, false
		}
		song.AlbumId = id
	} else {
		writeError(ctx, "album id wasn't provided", fasthttp.StatusBadRequest)
		return repo.Song{}, false
	}

	if arr, ok := values["release_year"]; ok {
		year, err := strconv.ParseInt(arr[0], 10, 16)
		if err != nil {
			writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
			return repo.Song{}, false
		}
		song.ReleaseYear = int16(year)
	}

	return song, true
}

func (h *HttpHandler) postMusic(ctx *fasthttp.RequestCtx) {
	form, err := ctx.MultipartForm()
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	song, ok := h.readSong(ctx, form.Value)
	if !ok {
		return
	}

	var dataArr []*multipart.FileHeader
	dataArr, ok = form.File["data"]
	if !ok {
		writeError(ctx, "data was not provided", fasthttp.StatusBadRequest)
		return
	}
	song.Data, song.Duration = h.readSongData(ctx, dataArr[0])
	if song.Data == nil {
		return
	}
	song.AddedAt = time.Now()

	err = h.songsTable.Insert(song)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

func (h *HttpHandler) getMusicAlbum(ctx *fasthttp.RequestCtx) {
	albumId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	var song []repo.Song
	song, err = h.songsTable.GetByAlbumId(albumId)
	if err != nil {
		if err == pgx.ErrNoRows {
			ctx.SetStatusCode(fasthttp.StatusNoContent)
			return
		}
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	var row []byte
	row, err = json.Marshal(song)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetContentType("applications/json")
	ctx.SetBody(row)
}

func (h *HttpHandler) getMusic(ctx *fasthttp.RequestCtx) {
	songId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	var song repo.Song
	song, err = h.songsTable.GetById(songId)
	if err != nil {
		if err == pgx.ErrNoRows {
			ctx.SetStatusCode(fasthttp.StatusNoContent)
			return
		}
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	var row []byte
	row, err = json.Marshal(song)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetContentType("applications/json")
	ctx.SetBody(row)
}

func (h *HttpHandler) deleteMusic(ctx *fasthttp.RequestCtx) {
	songId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	err = h.songsTable.DeleteById(songId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

type musicDataRequest struct {
	Id    int    `json:"id"`
	Start uint64 `json:"start"`
	End   uint64 `json:"end"`
}

func (h *HttpHandler) musicDataWriter(conn *websocket.Conn, requestChan chan musicDataRequest) chan struct{} {
	done := make(chan struct{})

	go func() {
		defer close(done)
		defer conn.Close()

		for {
			select {
			case <-time.After(pingPeriod):
				if err := conn.WriteMessage(websocket.PingMessage, nil); err != nil {
					return
				}

			case req, ok := <-requestChan:
				if ok {
					err := conn.SetWriteDeadline(time.Now().Add(idleWait))
					if err != nil {
						return
					}

					var data []byte
					data, err = h.musicService.Get(req.Id, req.Start, req.End)
					if err != nil {
						_ = conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseInternalServerErr, err.Error()))
					}

					err = conn.WriteMessage(websocket.BinaryMessage, data)
					if err != nil {
						return
					}
				} else {
					_ = conn.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseAbnormalClosure, ""))
					return
				}
			}
		}
	}()

	return done
}

func (h *HttpHandler) getMusicData(ctx *fasthttp.RequestCtx) {
	err := h.websocketsUpgrader.Upgrade(ctx, func(conn *websocket.Conn) {
		lastWrite := time.Now()
		err := conn.SetReadDeadline(lastWrite.Add(pongWait << 1))
		if err != nil {
			return
		}
		conn.SetPongHandler(func(string) error {
			_ = conn.SetReadDeadline(time.Now().Add(pongWait))
			return nil
		})
		err = conn.SetWriteDeadline(lastWrite.Add(idleWait))
		if err != nil {
			return
		}

		requestChan := make(chan musicDataRequest)

		done := h.musicDataWriter(conn, requestChan)

		defer func() {
			close(requestChan)
			<-done
		}()

		var (
			mt  int
			msg []byte
			req musicDataRequest
		)
		for {
			mt, msg, err = conn.ReadMessage()
			if err != nil {
				return
			}
			if mt != websocket.TextMessage {
				return
			}
			err = json.Unmarshal(msg, &req)
			if err != nil {
				return
			}

			requestChan <- req
		}
	})
	if err != nil {
		logrus.Warn(err.Error())
	}
}
