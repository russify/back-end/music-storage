package endpoint

import (
	"github.com/fasthttp/websocket"
	"github.com/sirupsen/logrus"
	"github.com/valyala/fasthttp"
	"music-storage/internal/auth"
	"music-storage/internal/mp3"
	"music-storage/internal/music"
	"music-storage/internal/repo"
	"music-storage/internal/util/cast"
	"time"
)

const (
	musicChunkSize = 131072
	idleWait       = time.Minute * 3
	pongWait       = time.Minute
	pingPeriod     = pongWait * 9 / 10
)

type HttpHandler struct {
	websocketsUpgrader websocket.FastHTTPUpgrader
	songDecorator      mp3.SongDecorator
	authorsTable       *repo.AuthorsTable
	albumsTable        *repo.AlbumsTable
	songsTable         *repo.SongsTable
	musicService       *music.Service
	authorizer         *auth.Authorizer
}

func NewHttpHandler(authorsTable *repo.AuthorsTable, albumsTable *repo.AlbumsTable, songsTable *repo.SongsTable, musicService *music.Service, authorizer *auth.Authorizer) *HttpHandler {
	return &HttpHandler{
		websocketsUpgrader: websocket.FastHTTPUpgrader{
			WriteBufferSize: musicChunkSize,
			ReadBufferSize:  musicChunkSize,
			CheckOrigin: func(ctx *fasthttp.RequestCtx) bool {
				return true
			},
		},
		authorsTable: authorsTable,
		albumsTable:  albumsTable,
		songsTable:   songsTable,
		musicService: musicService,
		authorizer:   authorizer,
	}
}

func (h *HttpHandler) Handle(ctx *fasthttp.RequestCtx) {
	defer func() {
		err := recover()
		if err != nil {
			logrus.Error(err)
			ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		}
	}()

	switch cast.ByteArrayToString(ctx.Path()) {

	case "/api/v1/authors":
		switch cast.ByteArrayToString(ctx.Method()) {
		case fasthttp.MethodPost:
			if h.authorizer.Authorize(ctx, auth.PermissionAddAuthor) {
				h.postAuthors(ctx)
			}

		case fasthttp.MethodGet:
			h.getAuthors(ctx)

		case fasthttp.MethodDelete:
			if h.authorizer.Authorize(ctx, auth.PermissionDeleteAuthor) {
				h.deleteAuthors(ctx)
			}

		default:
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/albums":
		switch cast.ByteArrayToString(ctx.Method()) {
		case fasthttp.MethodPost:
			if h.authorizer.Authorize(ctx, auth.PermissionAddAlbum) {
				h.postAlbums(ctx)
			}

		case fasthttp.MethodGet:
			h.getAlbums(ctx)

		case fasthttp.MethodDelete:
			if h.authorizer.Authorize(ctx, auth.PermissionDeleteAlbum) {
				h.deleteAlbums(ctx)
			}

		default:
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/albums/author":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodGet {
			h.getAlbumsAuthor(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/music":
		switch cast.ByteArrayToString(ctx.Method()) {
		case fasthttp.MethodPost:
			if h.authorizer.Authorize(ctx, auth.PermissionAddMusic) {
				h.postMusic(ctx)
			}

		case fasthttp.MethodGet:
			h.getMusic(ctx)

		case fasthttp.MethodDelete:
			if h.authorizer.Authorize(ctx, auth.PermissionDeleteMusic) {
				h.deleteMusic(ctx)
			}

		default:
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/music/data":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodGet {
			h.getMusicData(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	case "/api/v1/music/album":
		if cast.ByteArrayToString(ctx.Method()) == fasthttp.MethodGet {
			h.getMusicAlbum(ctx)
		} else {
			ctx.SetStatusCode(fasthttp.StatusMethodNotAllowed)
		}

	default:
		ctx.SetStatusCode(fasthttp.StatusNotFound)
	}
}
