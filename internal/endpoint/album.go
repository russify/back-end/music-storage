package endpoint

import (
	"encoding/json"
	"github.com/valyala/fasthttp"
	"music-storage/internal/dto"
	"music-storage/internal/repo"
)

func (h *HttpHandler) getAlbums(ctx *fasthttp.RequestCtx) {
	albumId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	var album repo.Album
	album, err = h.albumsTable.GetById(albumId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	albumDto := dto.Album{}
	albumDto.From(album)

	var row []byte
	row, err = json.Marshal(albumDto)
	ctx.SetContentType("application/json")
	ctx.SetBody(row)
}

func (h *HttpHandler) postAlbums(ctx *fasthttp.RequestCtx) {
	albumDto := dto.Album{}
	err := json.Unmarshal(ctx.PostBody(), &albumDto)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	if albumDto.Name == "" {
		writeError(ctx, "album name was not provided", fasthttp.StatusBadRequest)
		return
	}
	if albumDto.AuthorId == 0 {
		writeError(ctx, "invalid author id", fasthttp.StatusBadRequest)
		return
	}

	var album repo.Album
	album, err = albumDto.ToRepoModel()
	err = h.albumsTable.Insert(album)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusNoContent)
}

func (h *HttpHandler) deleteAlbums(ctx *fasthttp.RequestCtx) {
	albumId, err := ctx.QueryArgs().GetUint("id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	err = h.albumsTable.Delete(albumId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetStatusCode(fasthttp.StatusOK)
}

func (h *HttpHandler) getAlbumsAuthor(ctx *fasthttp.RequestCtx) {
	authorId, err := ctx.QueryArgs().GetUint("author_id")
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusBadRequest)
		return
	}

	var albums []repo.Album
	albums, err = h.albumsTable.GetByAuthorId(authorId)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	albumsDto := make([]dto.Album, len(albums))
	for i := range albumsDto {
		albumsDto[i].From(albums[i])
	}

	var row []byte
	row, err = json.Marshal(albumsDto)
	if err != nil {
		writeError(ctx, err.Error(), fasthttp.StatusInternalServerError)
		return
	}

	ctx.SetContentType("application/json")
	ctx.SetBody(row)
}
