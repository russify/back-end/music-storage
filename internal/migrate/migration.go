package migrate

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"github.com/jackc/pgx"
	"io"
	"music-storage/internal/util/cast"
	"os"
	"sort"
	"strconv"
	"strings"
)

var (
	ErrInvalidFileName             = errors.New("invalid migration file name")
	ErrInvalidHashForOldMigrations = errors.New("invalid migrations for old migrations")
)

type version struct {
	major int
	minor int
	str   string
}

func (v *version) parse(str string) bool {
	if len(str) < 4 && str[0] != 'V' {
		return false
	}
	majorEndIndex := strings.Index(str, ".")

	var err error
	v.major, err = strconv.Atoi(str[1:majorEndIndex])
	if err != nil {
		return false
	}

	v.minor, err = strconv.Atoi(str[majorEndIndex+1:])
	if err != nil {
		return false
	}

	v.str = str
	return true
}

func (v *version) Less(other version) bool {
	if v.major < other.major {
		return true
	}
	if v.major > other.minor {
		return false
	}
	if v.minor < other.minor {
		return true
	}
	return false
}

type migrationFile struct {
	path string
	ver  version
}

type migrationFiles []migrationFile

func (v migrationFiles) Len() int {
	return len(v)
}

func (v migrationFiles) Swap(i, j int) {
	v[i], v[j] = v[j], v[i]
}

func (v migrationFiles) Less(i, j int) bool {
	return v[i].ver.Less(v[j].ver)
}

func readMigrationsDir(dir string) (migrationFiles, error) {
	dirEntries, err := os.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	var files migrationFiles
	for _, e := range dirEntries {
		if !e.IsDir() {
			name := e.Name()
			verIndex := strings.Index(name, "_")
			if verIndex == -1 {
				return nil, ErrInvalidFileName
			}

			ver := version{}
			ok := ver.parse(name[:verIndex])
			if !ok {
				return nil, ErrInvalidFileName
			}

			files = append(files, migrationFile{
				path: dir + name,
				ver:  ver,
			})
		}
	}

	sort.Sort(files)
	return files, nil
}

func checkOldMigrations(filesInfo migrationFiles, infos []migrationInfo) error {
	if len(infos) == 0 {
		return nil
	}

	var buf [262_144]byte // 256Kb

	for index, info := range infos {
		fInfo := filesInfo[index]
		hasher := sha256.New()

		file, err := os.OpenFile(fInfo.path, os.O_RDONLY, 0666)
		if err != nil {
			return err
		}

		for err == nil {
			var n int
			n, err = file.Read(buf[:])
			hasher.Write(buf[:n])
		}

		_ = file.Close()
		if err != io.EOF {
			return err
		}

		var hash []byte
		hash, err = hex.DecodeString(info.hash)

		if !bytes.Equal(hasher.Sum(nil), hash) {
			return ErrInvalidHashForOldMigrations
		}
	}

	return nil
}

func applyMigration(table migrationsTable, file migrationFile) error {
	row, err := os.ReadFile(file.path)
	if err != nil {
		return err
	}

	_, err = table.dbConn.Exec(cast.ByteArrayToString(row))
	if err != nil {
		return err
	}

	hasher := sha256.New()
	_, err = hasher.Write(row)
	if err != nil {
		return err
	}

	return table.insertMigrations(migrationInfo{
		id:      0,
		version: file.ver.str,
		hash:    hex.EncodeToString(hasher.Sum(nil)),
	})
}

func decoratePath(dir string) string {
	if dir[len(dir)-1] != '/' {
		return dir + "/"
	}
	return dir
}

func Migrate(dbConn *pgx.Conn, dir string) error {
	dir = decoratePath(dir)

	files, err := readMigrationsDir(dir)
	if err != nil {
		return err
	}

	table := newMigrationsTable(dbConn)

	err = table.createTableIfNotExists()
	if err != nil {
		return err
	}

	var info []migrationInfo
	info, err = table.getAllMigrations()
	if err != nil {
		return err
	}

	err = checkOldMigrations(files, info)
	if err != nil {
		return err
	}

	for _, f := range files[len(info):] {
		err = applyMigration(table, f)
		if err != nil {
			return err
		}
	}

	return nil
}
