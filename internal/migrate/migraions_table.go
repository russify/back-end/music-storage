package migrate

import (
	"github.com/jackc/pgx"
)

const (
	createMigrationsTable = `
CREATE TABLE IF NOT EXISTS migrations (
	id SERIAL PRIMARY KEY,
	version VARCHAR(256) UNIQUE,
    hash VARCHAR(64) NOT NULL
)`

	getAllMigrations = `SELECT id, version, hash FROM migrations ORDER BY id`
	insertMigrations = `INSERT INTO migrations (version, hash) VALUES ($1, $2)`
)

type migrationsTable struct {
	dbConn *pgx.Conn
}

func newMigrationsTable(dbConn *pgx.Conn) migrationsTable {
	return migrationsTable{dbConn: dbConn}
}

type migrationInfo struct {
	id      int
	version string
	hash    string
}

func (t *migrationsTable) createTableIfNotExists() error {
	_, err := t.dbConn.Exec(createMigrationsTable)
	if err != nil {
		return err
	}
	return nil
}

func (t *migrationsTable) getAllMigrations() ([]migrationInfo, error) {
	rows, err := t.dbConn.Query(getAllMigrations)
	if err != nil {
		return nil, err
	}

	var result []migrationInfo
	for rows.Next() {
		var info migrationInfo

		err = rows.Scan(&info.id, &info.version, &info.hash)
		if err != nil {
			return nil, err
		}
		result = append(result, info)
	}
	rows.Close()

	return result, rows.Err()
}

func (t *migrationsTable) insertMigrations(info migrationInfo) error {
	_, err := t.dbConn.Exec(insertMigrations, info.version, info.hash)
	return err
}
