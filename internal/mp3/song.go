package mp3

import (
	"bytes"
	"errors"
	"github.com/bogem/id3v2/v2"
)

var ErrInvalidMp3File = errors.New("given mp3 file is invalid")

const (
	AudioVersionMpeg2_5 = iota
	AudioVersionUnused
	AudioVersionMpeg2
	AudioVersionMpeg1
	AllAudioVersionsNumber
)

const (
	LayerIII = iota + 1
	LayerII
	LayerI
	AllLayersNumber
)

const (
	tagHeaderSize   = 10
	frameHeaderSize = 4

	maxBitrateIndex      = 15
	maxSamplingRateIndex = 3
)

var LayerSlotSize = [AllLayersNumber]int{0, 1, 1, 4}

var Mpeg1BitrateTable = [AllLayersNumber][maxBitrateIndex]int{
	{},
	{0, 32000, 40000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000},
	{0, 32000, 48000, 56000, 64000, 80000, 96000, 112000, 128000, 160000, 192000, 224000, 256000, 320000, 384000},
	{0, 32000, 64000, 96000, 128000, 160000, 192000, 224000, 256000, 288000, 320000, 352000, 384000, 416000, 448000},
}

var Mpeg2And2_5BitrateTable = [AllLayersNumber][maxBitrateIndex]int{
	{},
	{0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160},
	{0, 8, 16, 24, 32, 40, 48, 56, 64, 80, 96, 112, 128, 144, 160},
	{0, 32, 48, 56, 64, 80, 96, 112, 128, 144, 160, 176, 192, 224, 256},
}

var SampleRatesTable = [AllAudioVersionsNumber][maxSamplingRateIndex]int{
	{11025, 12000, 8000},
	{},
	{22050, 24000, 16000},
	{44100, 48000, 32000},
}

// Samples per frame / 8 (in case of MPEG1 it's also divided with 4 (slot size))
var samplesPerFrameCoefficientTable = [AllAudioVersionsNumber][AllLayersNumber]int{
	{0, 72, 144, 12},
	{},
	{0, 72, 144, 12},
	{0, 144, 144, 12},
}

type Song struct {
	File   []byte
	Offset uint64
	Tag    *id3v2.Tag
}

func NewSong(file []byte) (*Song, error) {
	s := &Song{
		File: file,
		Tag:  id3v2.NewEmptyTag(),
	}

	ok := s.ResetOffset()
	if !ok {
		return nil, ErrInvalidMp3File
	}

	return s, s.Tag.Reset(bytes.NewBuffer(s.File), id3v2.Options{Parse: true})
}

type innerWriter []byte

func (w *innerWriter) Write(p []byte) (n int, err error) {
	*w = append(*w, p...)
	return len(p), nil
}

func (s *Song) SaveTagChanges() error {
	oldTagSize, ok := s.GetTagSize()
	if !ok {
		return ErrInvalidMp3File
	}

	buf := innerWriter{}
	_, err := s.Tag.WriteTo(&buf)
	if err != nil {
		return err
	}

	newTagSize := uint64(len(buf))
	if newSize := newTagSize + uint64(len(s.File)) - oldTagSize; newSize <= uint64(cap(s.File)) {
		if oldTagSize > newTagSize {
			copy(s.File, buf)
			s.File = append(s.File[:newTagSize], s.File[oldTagSize:]...)
		} else {
			s.File = append(s.File[:newTagSize], s.File[oldTagSize:]...)
			copy(s.File, buf)
		}
	} else {
		newFile := make([]byte, 0, newSize)
		newFile = append(newFile, buf...)
		s.File = append(newFile, s.File[oldTagSize:]...)
	}

	s.Offset = newTagSize

	return nil
}

func (s *Song) GetTagSize() (uint64, bool) {
	return s.parseTagSize(s.File[6:10])
}

func (s *Song) parseTagSize(header []byte) (uint64, bool) {
	var size uint64
	for _, b := range header {
		if b&0b1000_0000 == 1 {
			return 0, false
		}

		size = (size << 7) | uint64(b)
	}

	return size + tagHeaderSize, true
}

func (s *Song) getBitrate(mpegIndex, layerIndex, bitrateIndex byte) int {
	if mpegIndex == AudioVersionMpeg1 {
		return Mpeg1BitrateTable[layerIndex][bitrateIndex]
	}
	return Mpeg2And2_5BitrateTable[layerIndex][bitrateIndex]
}

func (s *Song) getPaddingSize(layerIndex, paddingBit byte) int {
	if paddingBit == 0 {
		return 0
	}

	return LayerSlotSize[layerIndex]
}

type FrameInfo struct {
	Size            uint64
	SamplesPerFrame int
	SampleRate      int
}

func (s *Song) NextFrame() (FrameInfo, bool) {
	if s.Offset < uint64(len(s.File)) {
		header := s.File[s.Offset : s.Offset+frameHeaderSize]
		if header[0] != 0xFF {
			return FrameInfo{}, false
		}

		mpegIndex := (header[1] & 0b0001_1000) >> 3
		layerIndex := (header[1] & 0b0000_0110) >> 1
		bitrateIndex := (header[2] & 0b1111_0000) >> 4
		sampleRateIndex := (header[2] & 0b0000_1100) >> 2
		paddingBit := (header[2] & 0b0000_0010) >> 1

		bitrate := s.getBitrate(mpegIndex, layerIndex, bitrateIndex)
		padding := s.getPaddingSize(layerIndex, paddingBit)
		samplesRate := SampleRatesTable[mpegIndex][sampleRateIndex]

		samplePerFrameCoef := samplesPerFrameCoefficientTable[mpegIndex][layerIndex]
		frameSize := uint64((samplePerFrameCoef*bitrate/samplesRate + padding) * LayerSlotSize[layerIndex])

		s.Offset += frameSize

		return FrameInfo{
			Size:            frameSize,
			SamplesPerFrame: samplePerFrameCoef << 3 * LayerSlotSize[layerIndex],
			SampleRate:      samplesRate,
		}, true
	}

	return FrameInfo{}, false
}

func (s *Song) ResetOffset() bool {
	var ok bool
	s.Offset, ok = s.GetTagSize()
	return ok
}
