package mp3

import (
	"encoding/binary"
	"github.com/bogem/id3v2/v2"
	"io"
	"math"
)

type SongMeta []uint64

func (ds SongMeta) GetOffsetForSecond(sec uint64) uint64 {
	if sec < uint64(len(ds)) {
		return ds[sec]
	}
	return 0
}

func (ds SongMeta) Length() uint64 {
	return uint64(len(ds)) - 1
}

type SongDecorator struct {
	tagId string
}

type metaFrame struct {
	frameForEachSecond []uint64
	size               int
}

func (f *metaFrame) Size() int {
	return f.size
}

func (f *metaFrame) UniqueIdentifier() string {
	return "RSSF"
}

func (f *metaFrame) WriteTo(w io.Writer) (int64, error) {
	var b [8]byte
	res := make([]byte, 0, f.size)
	slice := b[:8]
	binary.LittleEndian.PutUint64(slice, uint64(len(f.frameForEachSecond)))
	res = append(res, slice...)

	for _, offset := range f.frameForEachSecond {
		binary.LittleEndian.PutUint64(slice, offset)
		res = append(res, slice...)
	}

	remain := len(res)
	for remain != 0 {
		n, err := w.Write(res)
		if err != nil {
			return int64(len(res) - remain), err
		}
		remain -= n
	}
	return int64(len(res)), nil
}

func (d *SongDecorator) AddMeta(song *Song) (uint64, error) {
	var frameForEachSecond []uint64
	var length float64

	frameForEachSecond = append(frameForEachSecond, 0)
	startOffset := song.Offset
	info, ok := song.NextFrame()
	for ok {
		oldLength := length
		length += float64(info.SamplesPerFrame) / float64(info.SampleRate)
		if int(length)-int(oldLength) == 1 {
			floored := math.Floor(length)
			if length-floored > floored-oldLength {
				frameForEachSecond = append(frameForEachSecond, song.Offset-startOffset)
			} else {
				frameForEachSecond = append(frameForEachSecond, song.Offset-info.Size-startOffset)
			}
		}

		info, ok = song.NextFrame()
	}

	song.Tag.DeleteAllFrames()
	frame := metaFrame{
		frameForEachSecond: frameForEachSecond,
		size:               (len(frameForEachSecond) + 1) << 3,
	}

	tag := id3v2.NewEmptyTag()
	tag.AddFrame("RSSF", &frame)
	song.Tag = tag

	return uint64(len(frameForEachSecond)), song.SaveTagChanges()
}

func (d *SongDecorator) ReadMeta(song *Song) (SongMeta, error) {
	frame := song.Tag.GetFrames("RSSF")[0]

	buf := innerWriter(make([]byte, 0, frame.Size()))
	_, err := frame.WriteTo(&buf)
	if err != nil {
		return SongMeta{}, err
	}

	size := binary.LittleEndian.Uint64(buf)
	frameForEachSecond := make([]uint64, 0, size)
	for i := 8; i < len(buf); i += 8 {
		frameForEachSecond = append(frameForEachSecond, binary.LittleEndian.Uint64(buf[i:])+song.Offset)
	}

	return frameForEachSecond, nil
}
