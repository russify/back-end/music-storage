package auth

import (
	"github.com/valyala/fasthttp"
	"music-storage/internal/util/cast"
	"strconv"
)

const (
	PermissionAddAuthor = 1 << iota
	PermissionDeleteAuthor
	PermissionAddAlbum
	PermissionDeleteAlbum
	PermissionAddMusic
	PermissionDeleteMusic
)

const userRoleHeader = "User-Role"

const (
	UserRolePlainUser = iota
	UserRoleAdmin
	TotalUserRoles
)

type Authorizer struct {
	permissions [TotalUserRoles]byte
}

func NewAuthorizer() *Authorizer {
	permissions := [TotalUserRoles]byte{}
	permissions[UserRolePlainUser] = 0
	permissions[UserRoleAdmin] = PermissionAddAuthor | PermissionDeleteAuthor |
		PermissionAddAlbum | PermissionDeleteAlbum | PermissionAddMusic |
		PermissionDeleteMusic

	return &Authorizer{
		permissions: permissions,
	}
}

func (auth *Authorizer) Authorize(ctx *fasthttp.RequestCtx, permissions ...byte) bool {
	role, err := strconv.ParseUint(cast.ByteArrayToString(ctx.Request.Header.Peek(userRoleHeader)), 10, 16)
	if err != nil {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return false
	}
	if role < 0 || role >= TotalUserRoles {
		ctx.SetStatusCode(fasthttp.StatusInternalServerError)
		return false
	}

	for _, p := range permissions {
		if auth.permissions[role]&p == 0 {
			ctx.SetStatusCode(fasthttp.StatusForbidden)
			return false
		}
	}

	return true
}
